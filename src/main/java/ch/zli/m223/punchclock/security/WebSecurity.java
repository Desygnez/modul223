package ch.zli.m223.punchclock.security;

import static ch.zli.m223.punchclock.security.SecurityConstants.SIGN_UP_URL;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import ch.zli.m223.punchclock.user.UserDetailsServiceImpl;

/*
Class WebSecurity
@author Michelangelo Megna (Robin Bühler)
@Version 17.09.2021
*/

@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

	private UserDetailsServiceImpl userDetails;
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public WebSecurity(
			UserDetailsServiceImpl userDetailsService,
			BCryptPasswordEncoder bCryptPasswordEncoder
	) {
		this.userDetails = userDetailsService;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

    //Konfiguration der HTTPSecurity
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity
				.cors()
				.and()
				.csrf()
				.disable()
				.authorizeRequests()
				.antMatchers("/**/*.js","/**/*.html","/**/*.css")
				.permitAll()
				.antMatchers(HttpMethod.POST, SIGN_UP_URL)
				.permitAll()
				.antMatchers("/h2-console/**")
				.permitAll()
				.anyRequest()
				.authenticated()
				.and()
				.addFilter(new JWTAuthenticationFilter(authenticationManager()))
				.addFilter(new JWTAuthorizationFilter(authenticationManager()))
				// this disables session creation on Spring Security
				.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		httpSecurity.headers().frameOptions().disable();
	}

	@Override
	public void configure(
			AuthenticationManagerBuilder authenticationManagerBuilder
	) throws Exception {
		authenticationManagerBuilder
				.userDetailsService(userDetails)
				.passwordEncoder(bCryptPasswordEncoder);
	}

	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		final UrlBasedCorsConfigurationSource configurationSource
				= new UrlBasedCorsConfigurationSource();

		String[] headers = {
				"Access-Control-Allow-Headers",
				"Access-Control-Allow-Origin",
				"Access-COntrol-Expose-Headers",
				"Authorization",
				"Cache-Control",
				"Content-Type",
				"Origin"
		};

		CorsConfiguration corsConfiguration = new CorsConfiguration();
		corsConfiguration.applyPermitDefaultValues();
		for (String header : headers) {
			corsConfiguration.addExposedHeader(header);
		}
		corsConfiguration.addAllowedMethod("DELETE");
		corsConfiguration.addAllowedMethod("PUT");
		corsConfiguration.addAllowedMethod("OPTIONS");

		configurationSource
				.registerCorsConfiguration(
						"/**",
						corsConfiguration);
		return configurationSource;
	}

}
