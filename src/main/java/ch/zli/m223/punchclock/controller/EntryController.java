package ch.zli.m223.punchclock.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ch.zli.m223.punchclock.domain.Entry;
import ch.zli.m223.punchclock.service.EntryService;

/*
Class JobController
@author Michelangelo Megna
@Version 16.09.2021
*/

@RestController
@RequestMapping("/jobs")
public class EntryController {
    private EntryService EntryService;

    public EntryController(EntryService entryService) {
        this.EntryService = entryService;
    }

    /**
     * Alle Jobs werden zurückgegeben
     * @return AllJobs
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Entry> getAllJobs() {
        return EntryService.findAll();
    }

    /**
     * Erstellt einen neuen Eintrag/Job und gibt diesen zurück
     * @param job
     * @return job
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Entry createJob(@Valid @RequestBody Entry job) {
        return EntryService.createJob(job);
    }

    /**
     * Löscht einen Job mit id
     * @param id
     */
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteJob(@PathVariable Long id) {
		EntryService.deleteJob(id);
	}

    /**
     * Ändert einen Job und gibt diesen zurück
     * @param job
     * @return job
     */
    @PutMapping
	@ResponseStatus(HttpStatus.OK)
	public Entry updateJob(@Valid @RequestBody Entry job) {
		return EntryService.updateJob(job);
	}
}
