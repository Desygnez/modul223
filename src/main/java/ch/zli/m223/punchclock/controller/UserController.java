package ch.zli.m223.punchclock.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ch.zli.m223.punchclock.repository.ApplicationUserRepository;
import ch.zli.m223.punchclock.user.ApplicationUser;

/*
Class UserController
@author Michelangelo Megna
@Version 16.09.2021
*/


@RestController
@RequestMapping("/users")
public class UserController {

	private ApplicationUserRepository userRepository;
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public UserController(ApplicationUserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userRepository = userRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	/**
	 * Neuen Benutzer wird erstellt
	 * @param user
	 */
	@PostMapping("/sign-up")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void signUp(@RequestBody ApplicationUser user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userRepository.save(user);
	}

	/**
	 * Alle Benutzer zurückgeben
	 * @return allUsers
	 */
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public List<ApplicationUser> getAllUsers() {
		return userRepository.findAll();
	}

	/**
	 * Einen Benutzer nach Name zurückgeben
	 * @param username
	 * @return username
	 */
	@GetMapping("/{username}")
	@ResponseStatus(HttpStatus.OK)
	public ApplicationUser getUser(@PathVariable String username) {
		return userRepository.findbyUsername(username);
	}

	/**
	 * Löscht User nach id
	 * @param id
	 */
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteUser(@PathVariable Long id) {
		try {
			userRepository.deleteById(id);
		} catch (Exception e) {
			System.out.println("error");
		}
	}

	/**
	 * Daten des Users werden geändert
	 * @param user
	 * @return user
	 */
	@PutMapping
	@ResponseStatus(HttpStatus.OK)
	public ApplicationUser updateUser(@Valid @RequestBody ApplicationUser user) {
		return userRepository.saveAndFlush(user);
	}
}
