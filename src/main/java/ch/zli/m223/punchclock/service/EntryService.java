package ch.zli.m223.punchclock.service;
import java.util.List;
import org.springframework.stereotype.Service;
import ch.zli.m223.punchclock.domain.Entry;
import ch.zli.m223.punchclock.repository.EntryRepository;

/*
Class JobService
@author Michelangelo Megna (Robin Bühler)
@Version 16.09.2021
*/

@Service("JobService")
public class EntryService {
    private EntryRepository entryRepository;

    public EntryService(EntryRepository jobRepository) {
        this.entryRepository = jobRepository;
    }

    //Job wird erstellt und gespeichert
    public Entry createJob(Entry entry) {
        return entryRepository.saveAndFlush(entry);
    }

    //Alle Jobs finden und ausgeben
    public List<Entry> findAll() {
        return entryRepository.findAll();
    }

    //Job löschen
    public void deleteJob(long id) { entryRepository.deleteById(id); }

    //Job überschreiben
    public Entry updateJob(Entry entry) {
        return entryRepository.save(entry);
    }
}
