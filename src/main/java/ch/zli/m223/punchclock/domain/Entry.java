package ch.zli.m223.punchclock.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import ch.zli.m223.punchclock.user.*;
import javax.persistence.*;
import java.time.LocalDateTime;


/*
Class Job
@author Michelangelo Megna (Robin Bühler)
@Version 16.09.2021
*/

@javax.persistence.Entity
@Table(name = "entity")
public class Entry extends Entity {

	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@Column(nullable = false)
	private LocalDateTime checkIn;

	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@Column(nullable = false)
	private LocalDateTime checkOut;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private ApplicationUser user;

	public Entry(LocalDateTime checkIn, LocalDateTime checkOut, ApplicationUser user) {
		this.checkIn = checkIn;
		this.checkOut = checkOut;
		this.user = user;
	}
	public Entry() {}

	public LocalDateTime getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(LocalDateTime checkIn) {
		this.checkIn = checkIn;
	}

	public LocalDateTime getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(LocalDateTime checkOut) {
		this.checkOut = checkOut;
	}

	public void setUser(ApplicationUser user) {
		this.user = user;
	}

	public ApplicationUser user() {
		return user;
	}
}
