package ch.zli.m223.punchclock.repository;

import ch.zli.m223.punchclock.user.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;

/*
Class ApplicationUserRepository
@author Michelangelo Megna (Robin Bühler)
@Version 16.09.2021
*/

public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {
    ApplicationUser findbyUsername(String username);
}