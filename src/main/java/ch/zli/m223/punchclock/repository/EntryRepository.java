package ch.zli.m223.punchclock.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ch.zli.m223.punchclock.domain.Entry;
import ch.zli.m223.punchclock.user.ApplicationUser;
import org.springframework.data.domain.Sort;
import java.util.List;

@Repository
public interface EntryRepository extends JpaRepository<Entry, Long> {
    List<Entry> findAllbyUser(ApplicationUser user, Sort sort);
}
