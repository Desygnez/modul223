package ch.zli.m223.punchclock.user;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ch.zli.m223.punchclock.repository.*;
import static java.util.Collections.emptyList;

/*
Class UserDetailService
@author Michelangelo Megna (Robin Bühler)
@Version 16.09.2021
*/

@Service("UserDetailsService")
    public class UserDetailsServiceImpl implements UserDetailsService {
        private ApplicationUserRepository applicationUserRepository;

    public UserDetailsServiceImpl(ApplicationUserRepository applicationUserRepository) {
        this.applicationUserRepository = applicationUserRepository;
    }


//Username laden/finden
@Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ApplicationUser applicationUser = applicationUserRepository.findbyUsername(username);
            if (applicationUser == null) {
            throw new UsernameNotFoundException(username);
            }
        return new User(applicationUser.getUsername(), applicationUser.getPassword(), emptyList());
        }
    }