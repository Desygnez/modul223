package ch.zli.m223.punchclock.user;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import ch.zli.m223.punchclock.domain.Entity;
import ch.zli.m223.punchclock.domain.Entry;

/*
Class AppUser
@author Michelangelo Megna (Robin Bühler)
@Version 16.09.2021
*/

@javax.persistence.Entity
@Table(name = "users")
public class ApplicationUser extends Entity {

	@Column(nullable = false)
	private String username;

	@Column(nullable = false)
	private String password;

	@OneToMany(mappedBy = "user")
	private List<Entry> jobs;

	public ApplicationUser(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public ApplicationUser() {}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

    //Stellt Job/Entry ein
	public void setJobs(List<Entry> jobs) {
		this.jobs = jobs;
	}

    //Greift auf Job/Entry zu
	public List<Entry> getJobs() {
		return jobs;
	}

}
