const URL = 'http://localhost:8081';
const authHeader = localStorage.getItem('auth');
const username = localStorage.getItem('username');
let jobs = [];

const dateAndTimeToDate = (dateString, timeString) => {
    return new Date(`${dateString}T${timeString}`).toISOString();
};

const getUser = () => {
    fetch(`${URL}/user/${username}`, {
        method: 'GET',
        headers: {
            'Authorization': authHeader
        }
    }).then((result) => {
        result.json().then((user) => {
            return user;
        })
    })
}

//Job-Frontend wird angezeigt und ersellt Job
const createJob = (e) => {
    e.preventDefault();
    const formInfo = new FormData(e.target);
    const job = {};
    job['checkIn'] = dateAndTimeToDate(formInfo.get('checkInDate'), formInfo.get('checkInTime'));
    job['checkOut'] = dateAndTimeToDate(formInfo.get('checkOutDate'), formInfo.get('checkOutTime'));

    fetch(`${URL}/user/${username}`, {
        method: 'GET',
        headers: {
            'Authorization': authHeader
        }
    }).then((result) => {
        result.json().then((user) => {
            job['user'] = {'id': user.id};
            fetch(`${URL}/jobs`, {
                method: 'POST',
                headers: {
                    'Authorization': authHeader,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(job)
            }).then((result) => {
                result.json().then((job) => {
                    jobs.push(job);
                    renderJobs();
                });
            });
        })
    })
};


const indexJobs = () => {
    fetch(`${URL}/jobs`, {
        method: 'GET',
        headers: {
            'Authorization': authHeader,
        }
    }).then((result) => {
        result.json().then((result) => {
            jobs = result;
            renderJobs();
        });
    });
    renderJobs();
};

//Job-Frontend wird angezeigt und löscht Job
const deleteJob = (jobId) => {
    jobs = [];
    fetch(`${URL}/jobs/${jobId}`, {
        method: 'DELETE',
        headers: {
            'Authorization': authHeader,
        }
    }).then((result) => {
        if (result.status !== 204) {
            alert('A problem appeared while deleting');
        }
        indexJobs();
    });
};

const createCell = (text) => {
    const cell = document.createElement('td');
    cell.innerText = text;
    return cell;
};

const renderJobs = () => {
    const display = document.querySelector('#jobDisplay');
    display.innerHTML = '';
    jobs.forEach((job) => {
        const row = document.createElement('tr');
        row.appendChild(createCell(job.id));
        row.appendChild(createCell(new Date(job.checkIn).toLocaleString()));
        row.appendChild(createCell(new Date(job.checkOut).toLocaleString()));
        row.appendChild(createButton(job.id, 'Löschen', deleteJob));
        row.appendChild(createButton(job.id, 'Ändern', changeJob))
        display.appendChild(row);
    });
};

const createButton = (jobId, text, buttonFunction) => {
    const cell = document.createElement('td');
    const button = document.createElement('button');
    button.innerText = text;
    button.onclick = function () {
        console.log(jobId);
        buttonFunction(jobId);
    };
    cell.appendChild(button);
    return cell;
};

document.addEventListener('DOMContentLoaded', function(){
    const createJobForm = document.querySelector('#createJobForm');
    createJobForm.addEventListener('submit', createJob);
    indexJobs();
});
