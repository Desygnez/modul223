const URL = 'http://localhost:8081';
let isLoginForm = true;

const switchFormular = () => {
    let submitButton = document.getElementById('submitButton');
    let switchButton = document.getElementById('switchButton');
    let repPass = document.getElementById('repeatPassword');
    if (isLoginForm) {
        isLoginForm = false;
        submitButton.value = 'Create new account';
        switchButton.innerText = 'Already common here?';
        repPass.style.display = 'block'
    } else {
        isLoginForm = true;
        submitButton.value = 'Login';
        switchButton.innerText = 'First time here?';
        repPass.style.display = 'none';
    }
};

//Ein bereits registrierter User kann sich einloggen
const loginRegisterUser = (e) => {
    e.preventDefault();
    const formInfo = new FormData(e.target);
    const userInfo = {};
    userInfo['username'] = formInfo.get('username');
    userInfo['password'] = formInfo.get('password');
    if (userInfo['username'] === "" || userInfo['password'] === "") {
        return alert('Please fill all placeholders');
    }

    if (isLoginForm) {
        fetch(`${URL}/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userInfo)
        }).then((result) => {
            if (result.status !== 200) {
                return alert('Username or Password wrong.')
            }
            let authHeader = result.headers.get('Authorization');
            localStorage.setItem('auth', authHeader);
            localStorage.setItem('username', userInfo['username']);
        });
        window.location.href = `${URL}/jobs.html`;

    } else {
        if (userInfo['password'] !== formInfo.get('repeatPassword')) {
            return alert('Wrong password');
        }
        fetch(`${URL}/user/sign-up`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userInfo)
        }).then((result) => {
            if (result.status !== 202) {
                alert('The user was not created');
            } else {
                switchFormular();
                alert('The user was created')
            }
        });

    }

};



document.addEventListener('DOMContentLoaded', function () {
    const loginRegisterForm = document.getElementById('loginregisterForm');
    loginRegisterForm.addEventListener('submit', loginRegisterUser);
});
