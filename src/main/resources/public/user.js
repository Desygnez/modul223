const URL = 'http://localhost:8081';
const authHeader = localStorage.getItem('auth');
const username = localStorage.getItem('username');
let users = [];

//Login-Frontend wird angezeigt und ersellt User
const createUser = (e) => {
    e.preventDefault();
    const formInfo = new FormData(e.target);
    const userInfo = {};
    userInfo['username'] = formInfo.get('username');
    userInfo['password'] = formInfo.get('password');
    if (userInfo['username'] === "" || userInfo['password'] === "") {
        return alert('Please fill all placeholders');
    }
    if (userInfo['password'] !== formInfo.get('repeatPassword')) {
        return alert('Wrong password');
    }

    fetch(`${URL}/user/sign-up`, {
        method: 'POST',
        headers: {
            'Authorization': authHeader,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(userInfo)
    }).then((result) => {
        result.json().then((userInfo) => {
            users.push(userInfo);
            renderUsers();
        });
    });
};

//User-Frontend wird angezeigt und löscht User
const deleteUser = (userId) => {
    users = [];
    fetch(`${URL}/user/${userId}`, {
        method: 'DELETE',
        headers: {
            'Authorization': authHeader,
        }
    }).then((result) => {
        if (result.status !== 204) {
            alert('A problem appeared while deleting');
        }
        indexUsers();
    });
};

const indexUsers = () => {
    fetch(`${URL}/user`, {
        method: 'GET',
        headers: {
            'Authorization': authHeader,
        }
    }).then((result) => {
        result.json().then((result) => {
            users = result;
            renderUsers();
        });
    });
    renderUsers();
};

const createCell = (text) => {
    const cell = document.createElement('td');
    cell.innerText = text;
    return cell;
};

const createButton = (entryId, text, buttonFunction) => {
    const cell = document.createElement('td');
    const button = document.createElement('button');
    button.innerText = text;
    button.onclick = function () {
        console.log(entryId);
        buttonFunction(entryId);
    };
    cell.appendChild(button);
    return cell;
};

const renderUsers = () => {
    const display = document.querySelector('#userDisplay');
    display.innerHTML = '';
    users.forEach((user) => {
        const row = document.createElement('tr');
        row.appendChild(createCell(user.id));
        row.appendChild(createCell(user.username));
        row.appendChild(createButton(user.id, 'Löschen', deleteUser));
        row.appendChild(createButton(user, 'Ändern', changeUser));
        display.appendChild(row);
    });
};

document.addEventListener('DOMContentLoaded', function(){
    const createUserForm = document.querySelector('#createUserForm');
    createUserForm.addEventListener('submit', createUser);
    indexUsers();
});
