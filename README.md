**Modul223 : Punchclock**

Praktische Umsetzungsarbeit 223
Multi-User-Applikationen objektorientiert realisieren
Element c: Praktische Umsetzungsarbeit

**Funktionen:**

Benutzeroberfläche, mit der Sie sich an der Applikation registrieren und anmelden können.
Benutzeroberfläche, um Benutzer zu verwalten (erstellen und löschen)
Benutzeroberfläche, um eine spezifische Entität Ihrer Applikation zu verwalten (erstellen und löschen)

SQL-File data.sql ist ein Skript zum Einfügen von Beispieldaten (Token, Time)

**Starten der Applikation**

Folgende Schritte befolgen um loszulegen:
1. Sicherstellen, dass JDK 12 installiert und in der Umgebungsvariable `path` definiert ist.
2. Gradle Installieren und mit dem Projekt verbinden
3. Ins Verzeichnis der Applikation wechseln und über die Kommandozeile mit `./gradlew bootRun` oder `./gradlew.bat bootRun` starten
4. Nun sind sie auf der Webseite und sie können ein User erstellen
5. Unittest mit `./gradlew test` oder `./gradlew.bat test` ausführen.
6. Ein ausführbares JAR kann mit `./gradlew bootJar` oder `./gradlew.bat bootJar` erstellt werden.

Folgende Dienste stehen während der Ausführung im Profil `dev` zur Verfügung:
- REST-Schnittstelle der Applikation: http://localhost:8081
- Dashboard der H2 Datenbank: http://localhost:8081/h2-console


# README Info

## Author
Michelangelo Megna (Basisprojekt: Robin Bühler)

## Merge Request

Git: https://gitlab.com/Desygnez/modul223

## License
No commercial use