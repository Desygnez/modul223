INSERT INTO "PUBLIC"."USERS" VALUES
(1, '$2a$10$M2oHQ/0AybustUGVdxT8AeYipMeKp7g6brzhBoN9GA1N4SJrOWxKG', 'admin');
INSERT INTO "PUBLIC"."ENTRIES" VALUES
(2, TIMESTAMP '2021-09-16 00:21:00', TIMESTAMP '2021-10-17 06:45:00', 1),
(3, TIMESTAMP '2021-09-17 00:21:00', TIMESTAMP '2023-10-17 06:45:00', 1);
